# Express pressure loss as volume integral

[TOC]


## この文書について

- 流体機械で生じる損失を減らすために，損失がどこで生じているのかを確かめられるような理論的手法が欲しい．
- ここでは，入口-出口間の全圧損失を，領域の体積分で表す手法について調べる．
- そのような積分があれば，その被積分関数(integrand)の分布によって損失の要因を可視化できるはず．
- そうした考えをメモ・修正していくために作成した．

## 共通の仮定

- ニュートン流体

## 場合により選択する仮定

### 圧縮性

- 理想気体（polytropic）

### 非圧縮性

- 密度一定（比熱は？）

## 全圧

- 理論流体力学の書籍として，ここではWu,J.Z.らの"Vortial Flows"を参考にする．

- 全圧 $p_T$ は，「流れのある点において，状態が等エントロピー変化して速度0となったと仮想した際の圧力」といった風に解釈できる．
    - 「等エントロピー変化」というのは，後で全エンタルピーの項で触れる所の，流れエネルギーと運動エネルギーの総和が一定に保たれること，と見て良いのではないか．

- 非圧縮性流れでは，

$$
    p_T = p + \frac{\rho q^2}{2} . \space [ \mathrm{Pa = J / m^3} ]
$$

- 圧縮性流れでは，

$$
    p_T = \left( 1 + \frac{\gamma - 1}{2} M^2 \right)^{ \frac{\gamma}{\gamma - 1} } p . \space [ \mathrm{Pa = J / m^3} ]
$$

## 全エントロピー

- 全エンタルピー $H$ は，次式で表される[?]．

$$
    H = e + \frac{p}{\rho} + \frac{q^2}{2} . \space [ \mathrm{J/kg} ]
$$

- ここで， $e$ は内部エネルギー， $p$ は圧力， $\rho$ は密度， $q$ は速度絶対値である．
    - 右辺第二項，第三項はそれぞれ，流れエネルギー，運動エネルギーと呼ばれる．

- 流れが非圧縮性であれば，こう書き換えられるはずである．

$$
    H = e + \frac{p_T}{\rho} .
$$

- 流れが圧縮性で理想気体と見なすことができれば，こう書き換えられるはずである．

$$
    e = c_v T = \frac{R}{\gamma - 1} T = \frac{1}{\gamma - 1} \frac{p}{\rho} .
$$

$$
    \therefore H = \frac{\gamma}{\gamma - 1} \frac{p}{\rho} + \frac{q^2}{2} .
$$

- こうなると，圧力を考える際にもはや内部エネルギーと流れエネルギーとで分けて考えることができない．（非圧縮性流れでは，圧力が内部エネルギーに影響を与えることがなかった＝独立していた？）

- 疑問．流れが圧縮性であろうと，全エンタルピーが「内部エネルギー＋その他のエネルギー」という図式であることに変わりはないので，

## 全エンタルピーは保存される？

- いまいち書籍を読んでも理解できていないのでメモ．
- 全圧は急拡大や急縮小などの流れで顕著に低下する．これは粘性摩擦により熱が生じたため（＝流れエネルギーや運動エネルギーが内部エネルギーに変換されたため），で正しい？
- 対して全エンタルピーは，内部エネルギー，流れエネルギー，運動エネルギーの総和であるので，急拡大/急縮小管内の粘性流れにおいて，全エンタルピーは変化しないことになる．正しい？
- つまり，流体力学，特に内部流れで言う「損失」「圧力損失」という用語は，「流れエネルギーや運動エネルギーが内部エネルギーに変換されること」と言い換えられる．正しい？
- 「損失」というが，内部エネルギーへの変換は不可逆的に起こるのか？内部エネルギーが他のエネルギーに変換されることはないのか？熱機関とかがそうでは？

- ポアズイユ流れで検証．

